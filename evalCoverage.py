import json

try:
    from robot.libraries.BuiltIn import BuiltIn
    from robot.libraries.BuiltIn import _Misc
    import robot.api.logger as logger
    from robot.api.deco import keyword
    ROBOT = True
except Exception:
    ROBOT = False

@keyword("evalCoverage")
def getCoverage(coveragefile):
    with open(coveragefile) as user_file:
        file_contents = user_file.read()
    coverage_json = json.loads(file_contents)
    if coverage_json["coverage"] >= 85:
        return "PASS"
    else:
        return "FAIL"