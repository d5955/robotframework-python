import json
import sys

try:
    from robot.libraries.BuiltIn import BuiltIn
    from robot.libraries.BuiltIn import _Misc
    import robot.api.logger as logger
    from robot.api.deco import keyword
    ROBOT = True
except Exception as e:
    ROBOT = False

@keyword("evalDockerfile")
def getDockerfile(dockerfile):
    notHM = 0
    with open(dockerfile) as user_file:
        file_contents = user_file.readlines()
    for row in file_contents:
        if row.startswith("from"):
            if "artifactory.mtb.com" in row:
                pass
            else:
                notHM = 1
    if notHM == 0:
        return "PASS"
    else:
        return "FAIL"