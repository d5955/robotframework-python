*** Setting ***
Library  evalCoverage.py
Library  evalDockerfile.py

*** Variables ***
${coverfile}  coverage.json
${dockerfile}  Dockerfile

*** Test Cases ***
Evaluate Unit Test Result
    ${value}=  evalCoverage  ${coverfile}
    SHOULD BE EQUAL  ${value}  PASS

Evaluate Dockerfile
    ${value}=  evalDockerfile  ${dockerfile}
    SHOULD BE EQUAL  ${value}  PASS
